This is the repository of a CAD system for diabetic retinopathy detection for the course 'Computer aided diagnosis in medical imaging (NWI-IMC037-2014-KW3-V)'.

Authors: Jasper van Dalen (s4041437), Flip van Rijn (s4050614) & Mirjam van Nahmen (s3004120)

Info
====
The latest version of the MeVisLab network is **cad4.mlab**. The network uses custom modules and need to be compiled to be used. The source code of the custom modules are in the **modules** directory.