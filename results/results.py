import numpy as np
from IPython import embed

idx2level = []
idx2image = []

print 'Building idx2level dictionary...'
with open('cad4_train.csv') as f:
    for line in f:
        vals = line.split(',')
        idx2level.append(vals[1])

with open('cad4_test.csv') as f:
    for line in f:
        vals = line.split(',')
        idx2image.append(vals[0])

output = []

print 'Getting level for test set...'
with open('neighbours.csv') as f:
    for line in f:
        neighbours = line.split(',')
        levels = []
        for neighbour in neighbours:
            levels.append(idx2level[int(neighbour)])
        levels = np.array(levels).astype(np.float)
        output.append(np.median(levels))

with open('ouspus.csv', 'w') as f:
    f.write('image,level\n')
    for idx, level in enumerate(output):
        f.write('%s,%d\n' % (idx2image[idx], level))