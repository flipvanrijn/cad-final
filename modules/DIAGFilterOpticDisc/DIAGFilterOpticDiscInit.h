//----------------------------------------------------------------------------------
//! Dynamic library and runtime type system initialization.
/*!
// \file    
// \author  Flip
// \date    2015-05-14
*/
//----------------------------------------------------------------------------------


#ifndef __DIAGFilterOpticDiscInit_H
#define __DIAGFilterOpticDiscInit_H


ML_START_NAMESPACE

//! Calls init functions of all modules to add their types to the runtime type
//! system of the ML.
int DIAGFilterOpticDiscInit();

ML_END_NAMESPACE

#endif // __DIAGFilterOpticDiscInit_H
