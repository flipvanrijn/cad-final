# -----------------------------------------------------------------------------
# DIAGFilterOpticDisc project profile
#
# \file
# \author  Flip
# \date    2015-05-14
# -----------------------------------------------------------------------------


TEMPLATE   = lib

TARGET     = DIAGFilterOpticDisc

DESTDIR    = ../../../lib
DLLDESTDIR = ../../../lib

# Set high warn level (warn 4 on MSVC)
WARN = HIGH

# Add used projects here (see included pri files below for available projects)
CONFIG += dll ML

MLAB_PACKAGES += DIAG_CADStudent \
                 MeVisLab_Standard

# make sure that this file is included after CONFIG and MLAB_PACKAGES
include ($(MLAB_MeVis_Foundation)/Configuration/IncludePackages.pri)

DEFINES += DIAGFILTEROPTICDISC_EXPORTS

# Enable ML deprecated API warnings. To completely disable the deprecated API, change WARN to DISABLE.
DEFINES += ML_WARN_DEPRECATED

HEADERS += \
    DIAGFilterOpticDiscInit.h \
    DIAGFilterOpticDiscSystem.h \
    mlFilterOpticDisc.h \

SOURCES += \
    DIAGFilterOpticDiscInit.cpp \
    mlFilterOpticDisc.cpp \