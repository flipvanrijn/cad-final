//----------------------------------------------------------------------------------
//! Project global and OS specific declarations.
/*!
// \file    
// \author  Flip
// \date    2015-05-14
*/
//----------------------------------------------------------------------------------


#ifndef __DIAGFilterOpticDiscSystem_H
#define __DIAGFilterOpticDiscSystem_H


// DLL export macro definition.
#ifdef DIAGFILTEROPTICDISC_EXPORTS
  // Use the DIAGFILTEROPTICDISC_EXPORT macro to export classes and functions.
  #define DIAGFILTEROPTICDISC_EXPORT ML_LIBRARY_EXPORT_ATTRIBUTE
#else
  // If included by external modules, exported symbols are declared as import symbols.
  #define DIAGFILTEROPTICDISC_EXPORT ML_LIBRARY_IMPORT_ATTRIBUTE
#endif


#endif // __DIAGFilterOpticDiscSystem_H
