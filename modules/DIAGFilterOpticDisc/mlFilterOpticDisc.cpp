//----------------------------------------------------------------------------------
//! The ML module class FilterOpticDisc.
/*!
// \file   
// \author  Flip
// \date    2015-05-14
//
// 
*/
//----------------------------------------------------------------------------------

// Local includes
#include "mlFilterOpticDisc.h"
#include <vector>
#include <map>


ML_START_NAMESPACE

const int dx[] = { +1, 0, -1, 0 };
const int dy[] = { 0, +1, 0, -1 };
std::vector<std::vector<int>> labels;

//! Implements code for the runtime type system of the ML
ML_MODULE_CLASS_SOURCE(FilterOpticDisc, Module);

//----------------------------------------------------------------------------------

FilterOpticDisc::FilterOpticDisc() : Module(1, 1)
{
  // Suppress calls of handleNotification on field changes to
  // avoid side effects during initialization phase.
  handleNotificationOff();

  // Reactivate calls of handleNotification on field changes.
  handleNotificationOn();


  // Activate inplace data buffers for output outputIndex and input inputIndex.
  // setOutputImageInplace(outputIndex, inputIndex);

  // Activate page data bypass from input inputIndex to output outputIndex.
  // Note that the module must still be able to calculate the output image.
  // setBypass(outputIndex, inputIndex);
}

//----------------------------------------------------------------------------------

void FilterOpticDisc::handleNotification(Field* field)
{
  // Handle changes of module parameters and input image fields here.
  bool touchOutputs = false;
  if (isInputImageField(field))
  {
    touchOutputs = true;
  }

  if (touchOutputs) 
  {
    // Touch all output image fields to notify connected modules.
    touchOutputImageFields();
  }
}

//----------------------------------------------------------------------------------

void FilterOpticDisc::calculateOutputImageProperties(int /*outputIndex*/, PagedImage* outputImage)
{
  // Change properties of output image outputImage here whose
  // defaults are inherited from the input image 0 (if there is one).
}

//----------------------------------------------------------------------------------

SubImageBox FilterOpticDisc::calculateInputSubImageBox(int inputIndex, const SubImageBox& outputSubImageBox, int outputIndex)
{
  // Return region of input image inputIndex needed to compute region
  // outSubImgBox of output image outputIndex.
  return outputSubImageBox;
}


//----------------------------------------------------------------------------------

ML_CALCULATEOUTPUTSUBIMAGE_NUM_INPUTS_1_CPP(FilterOpticDisc);

template <typename T>
void FilterOpticDisc::calculateOutputSubImage(TSubImage<T>* outputSubImage, int outputIndex
                                     , TSubImage<T>* inputSubImage0
                                     )
{
  // Compute sub-image of output image outputIndex from input sub-images.
  std::map<int, int> hist = this->calculateHistogram(inputSubImage0);
  std::map<int, int>::reverse_iterator rit = hist.rbegin();

  int maxIntensity = rit->first;
  double t1 = maxIntensity;
  double t2 = 0;

  for (rit = hist.rbegin(); rit != hist.rend(); ++rit) {
    if (rit != hist.rbegin() && rit->second > 1000) {
      t2 = rit->first;
      break;
    }
  }

  double T_k = (t1 + t2) / 2;
  double T_k1 = 0;

  double m_bk = this->meanLevel(inputSubImage0, 0, t2 - 1);
  double m_ok= this->meanLevel(inputSubImage0, t2, maxIntensity);

  while (T_k1 != T_k) {
    m_bk = this->meanLevel(inputSubImage0, 0, m_ok - 1);
    m_ok = this->meanLevel(inputSubImage0, m_ok, maxIntensity);
    T_k = T_k1;
    T_k1 = (m_bk + m_ok) / 2;
  }

  int component = 0;

  // Clamp box of output image against image extent to avoid that unused areas are processed.
  const SubImageBox validOutBox = outputSubImage->getValidRegion();
  labels.resize(validOutBox.v2.y + 1, std::vector<int>(validOutBox.v2.x + 1, 0));

  // Process all voxels of the valid region of the output page.
  ImageVector p;
  for (p.u=validOutBox.v1.u;  p.u<=validOutBox.v2.u;  ++p.u) {
    for (p.t=validOutBox.v1.t;  p.t<=validOutBox.v2.t;  ++p.t) {
      for (p.c=validOutBox.v1.c;  p.c<=validOutBox.v2.c;  ++p.c) {
        for (p.z=validOutBox.v1.z;  p.z<=validOutBox.v2.z;  ++p.z) {
          for (p.y=validOutBox.v1.y;  p.y<=validOutBox.v2.y;  ++p.y) {

            p.x = validOutBox.v1.x;
            // Get pointers to row starts of input and output sub-images.
            const T* inVoxel0 = inputSubImage0->getImagePointer(p);

            T*  outVoxel = outputSubImage->getImagePointer(p);

            const MLint rowEnd   = validOutBox.v2.x;

            // Process all row voxels.
            for (; p.x <= rowEnd;  ++p.x, ++outVoxel, ++inVoxel0)
            {
              if (*inVoxel0 >= T_k1) {
                *outVoxel = 1;
              }
              else {
                *outVoxel = 0;
              }
            }
          }
        }
      }
    }
  }

  /*for (p.u = validOutBox.v1.u; p.u <= validOutBox.v2.u; ++p.u) {
    for (p.t = validOutBox.v1.t; p.t <= validOutBox.v2.t; ++p.t) {
      for (p.c = validOutBox.v1.c; p.c <= validOutBox.v2.c; ++p.c) {
        for (p.z = validOutBox.v1.z; p.z <= validOutBox.v2.z; ++p.z) {
          for (p.y = validOutBox.v1.y; p.y <= validOutBox.v2.y; ++p.y) {

            p.x = validOutBox.v1.x;
            // Get pointers to row starts of input and output sub-images.
            const T* inVoxel0 = inputSubImage0->getImagePointer(p);

            T*  outVoxel = outputSubImage->getImagePointer(p);

            const MLint rowEnd = validOutBox.v2.x;

            // Process all row voxels.
            for (; p.x <= rowEnd; ++p.x, ++outVoxel, ++inVoxel0)
            {
              *outVoxel = this->labels[p.y][p.x];
            }
          }
        }
      }
    }
  }*/
}

template <typename T>
void FilterOpticDisc::dfs(TSubImage<T>* inputSubImage0, double T_k1, int x, int y, int currentLabel)
{
  if (x < 0 || x > labels[0].size() - 1){
    //std::cout << "x out of bounds" << x << "," << this->labels[0].size() << std::endl;
    return;
  }
  if (y < 0 || y > labels.size() - 1) {
    //std::cout << "y out of bounds" << y << "," << this->labels.size() << std::endl;
    return;
  }

  T* inVoxel = inputSubImage0->getImagePointer(ImageVector(x, y, 0, 0, 0, 0));

  if (labels[y][x] || *inVoxel < T_k1) return;
 /* for (int direction = 0; direction < 4; direction++) {
    this->dfs(inputSubImage0, T_k1, x + dx[direction], y + dy[direction], currentLabel);
  }*/
  this->dfs(inputSubImage0, T_k1, x + dx[0], y + dy[0], currentLabel);
  this->dfs(inputSubImage0, T_k1, x + dx[1], y + dy[1], currentLabel);
  this->dfs(inputSubImage0, T_k1, x + dx[2], y + dy[2], currentLabel);
  this->dfs(inputSubImage0, T_k1, x + dx[3], y + dy[3], currentLabel);
}

template <typename T>
std::map<int, int> FilterOpticDisc::calculateHistogram(TSubImage<T>* inputSubImage0)
{
  std::map<int, int> _histogram;

  const SubImageBox validOutBox = inputSubImage0->getValidRegion();

  // Process all voxels of the valid region of the output page.
  ImageVector p;
  for (p.u = validOutBox.v1.u; p.u <= validOutBox.v2.u; ++p.u) {
    for (p.t = validOutBox.v1.t; p.t <= validOutBox.v2.t; ++p.t) {
      for (p.c = validOutBox.v1.c; p.c <= validOutBox.v2.c; ++p.c) {
        for (p.z = validOutBox.v1.z; p.z <= validOutBox.v2.z; ++p.z) {
          for (p.y = validOutBox.v1.y; p.y <= validOutBox.v2.y; ++p.y) {

            p.x = validOutBox.v1.x;
            // Get pointers to row starts of input and output sub-images.
            const T* inVoxel0 = inputSubImage0->getImagePointer(p);

            const MLint rowEnd = validOutBox.v2.x;

            // Process all row voxels.
            for (; p.x <= rowEnd; ++p.x, ++inVoxel0)
            {
              std::map<int, int>::iterator it = _histogram.find(*inVoxel0);
              if (it != _histogram.end())
                _histogram[*inVoxel0] = _histogram[*inVoxel0] + 1;
              else
                _histogram[*inVoxel0] = 1;
            }
          }
        }
      }
    }
  }

  return _histogram;
}

template <typename T>
double FilterOpticDisc::meanLevel(TSubImage<T>* inputSubImage0, double lowerThreshold, double upperThreshold)
{
  double sum = 0;
  double counter = 0;

  const SubImageBox validOutBox = inputSubImage0->getValidRegion();

  // Process all voxels of the valid region of the output page.
  ImageVector p;
  for (p.u = validOutBox.v1.u; p.u <= validOutBox.v2.u; ++p.u) {
    for (p.t = validOutBox.v1.t; p.t <= validOutBox.v2.t; ++p.t) {
      for (p.c = validOutBox.v1.c; p.c <= validOutBox.v2.c; ++p.c) {
        for (p.z = validOutBox.v1.z; p.z <= validOutBox.v2.z; ++p.z) {
          for (p.y = validOutBox.v1.y; p.y <= validOutBox.v2.y; ++p.y) {

            p.x = validOutBox.v1.x;
            // Get pointers to row starts of input and output sub-images.
            const T* inVoxel0 = inputSubImage0->getImagePointer(p);

            const MLint rowEnd = validOutBox.v2.x;

            // Process all row voxels.
            for (; p.x <= rowEnd; ++p.x, ++inVoxel0)
            {
              if (*inVoxel0 >= lowerThreshold && *inVoxel0 <= upperThreshold) {
                sum += *inVoxel0;
                counter++;
              }
            }
          }
        }
      }
    }
  }

  return (sum / counter);
}

ML_END_NAMESPACE