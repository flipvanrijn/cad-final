//----------------------------------------------------------------------------------
//! Project global and OS specific declarations.
/*!
// \file    
// \author  Jasper
// \date    2015-06-12
*/
//----------------------------------------------------------------------------------


#ifndef __DIAGAreaGrowSystem_H
#define __DIAGAreaGrowSystem_H


// DLL export macro definition.
#ifdef DIAGAREAGROW_EXPORTS
  // Use the DIAGAREAGROW_EXPORT macro to export classes and functions.
  #define DIAGAREAGROW_EXPORT ML_LIBRARY_EXPORT_ATTRIBUTE
#else
  // If included by external modules, exported symbols are declared as import symbols.
  #define DIAGAREAGROW_EXPORT ML_LIBRARY_IMPORT_ATTRIBUTE
#endif


#endif // __DIAGAreaGrowSystem_H
