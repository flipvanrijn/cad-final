//----------------------------------------------------------------------------------
//! The ML module class AreaGrow.
/*!
// \file   
// \author  Jasper
// \date    2015-06-12
//
// 
*/
//----------------------------------------------------------------------------------

// Local includes
#include "mlAreaGrow.h"
#include "vector"
#include <tuple>

ML_START_NAMESPACE

//! Implements code for the runtime type system of the ML
ML_MODULE_CLASS_SOURCE(AreaGrow, Module);

//----------------------------------------------------------------------------------

AreaGrow::AreaGrow() : Module(1, 1)
{
  // Suppress calls of handleNotification on field changes to
  // avoid side effects during initialization phase.
  handleNotificationOff();

  // Reactivate calls of handleNotification on field changes.
  handleNotificationOn();


  // Activate inplace data buffers for output outputIndex and input inputIndex.
  // setOutputImageInplace(outputIndex, inputIndex);

  // Activate page data bypass from input inputIndex to output outputIndex.
  // Note that the module must still be able to calculate the output image.
  // setBypass(outputIndex, inputIndex);

}

//----------------------------------------------------------------------------------
bool sortFunction(std::vector<std::tuple<int, int, MLint>> a, std::vector<std::tuple<int, int, MLint>> b){ return std::get<2>(a[0]) < std::get<2>(b[0]); }

void AreaGrow::handleNotification(Field* field)
{
  // Handle changes of module parameters and input image fields here.
  bool touchOutputs = false;
  if (isInputImageField(field))
  {
    touchOutputs = true;
  }

  if (touchOutputs) 
  {
    // Touch all output image fields to notify connected modules.
    touchOutputImageFields();
  }
}

//----------------------------------------------------------------------------------

void AreaGrow::calculateOutputImageProperties(int /*outputIndex*/, PagedImage* outputImage)
{
  // Change properties of output image outputImage here whose
  // defaults are inherited from the input image 0 (if there is one).
}

//----------------------------------------------------------------------------------

SubImageBox AreaGrow::calculateInputSubImageBox(int inputIndex, const SubImageBox& outputSubImageBox, int outputIndex)
{
  // Return region of input image inputIndex needed to compute region
  // outSubImgBox of output image outputIndex.
  return outputSubImageBox;
}


//----------------------------------------------------------------------------------

ML_CALCULATEOUTPUTSUBIMAGE_NUM_INPUTS_1_CPP(AreaGrow);

template <typename T>
void AreaGrow::calculateOutputSubImage(TSubImage<T>* outputSubImage, int outputIndex
                                     , TSubImage<T>* inputSubImage0
                                     )
{
  // Compute sub-image of output image outputIndex from input sub-images.

  // Clamp box of output image against image extent to avoid that unused areas are processed.
  const SubImageBox validOutBox = outputSubImage->getValidRegion();

  // Process all voxels of the valid region of the output page.
  ImageVector p;
  int counter = 1;
  int threshold =3;
  std::vector<MLint> value;
  std::vector < std::vector < std::tuple <int,int, MLint> > >  areas;
  std::vector<std::tuple<int,int, MLint>> dummy;
  //std::vector<std::vector<int>> OutputImage;
  areas.push_back(dummy); // push een lege eersteplaats zodat counter op 1 kan blijven en positie in vector is
  for (p.u = validOutBox.v1.u; p.u <= validOutBox.v2.u; ++p.u) {
	  for (p.t = validOutBox.v1.t; p.t <= validOutBox.v2.t; ++p.t) {
		  for (p.c = validOutBox.v1.c; p.c <= validOutBox.v2.c; ++p.c) {
			  for (p.z = validOutBox.v1.z; p.z <= validOutBox.v2.z; ++p.z) {
				  for (p.y = validOutBox.v1.y; p.y <= validOutBox.v2.y; ++p.y) {
					  p.x = validOutBox.v1.x;
					  // Get pointers to row starts of input and output sub-images.
					  const T* inVoxel0 = inputSubImage0->getImagePointer(p);

					  T*  outVoxel = outputSubImage->getImagePointer(p);

					  const MLint rowEnd = validOutBox.v2.x;

					  std::vector<int> a;
//					  OutputImage.push_back(a);
					  // Process all row voxels.
					  for (; p.x <= rowEnd; ++p.x, ++outVoxel, ++inVoxel0)
					  {
						  *outVoxel = 0;
						 // OutputImage[p.y].push_back(0);
					  }
				  }
			  }
		  }
	  }
  }

  for (p.u=validOutBox.v1.u;  p.u<=validOutBox.v2.u;  ++p.u) {
    for (p.t=validOutBox.v1.t;  p.t<=validOutBox.v2.t;  ++p.t) {
      for (p.c=validOutBox.v1.c;  p.c<=validOutBox.v2.c;  ++p.c) {
        for (p.z=validOutBox.v1.z;  p.z<=validOutBox.v2.z;  ++p.z) {
          for (p.y=validOutBox.v1.y;  p.y<=validOutBox.v2.y;  ++p.y) {

            p.x = validOutBox.v1.x;
            // Get pointers to row starts of input and output sub-images.
            const T* inVoxel0 = inputSubImage0->getImagePointer(p);

            T*  outVoxel = outputSubImage->getImagePointer(p);

            const MLint rowEnd   = validOutBox.v2.x;

            // Process all row voxels.
            for (; p.x <= rowEnd;  ++p.x, ++outVoxel, ++inVoxel0)
            {
				if (p.x != 0 && p.x != (rowEnd - 1) && p.y != 0 && p.y != validOutBox.v2.y)
				{
					
					for (int x = 0; x < 3; x++)
					{
						for (int y = 0; y < 3; y++)
						{
							if (p.x ==350 && p.y == 200 && *inVoxel0>0)
								int a = 2; //plusminus midden plaatje
							T*  kernelVoxel1 = inputSubImage0->getImagePointer(ImageVector((p.x - 1) + x, (p.y - 1) + y, 0, 0, 0, 0));
							T*  kernelVoxel2 = outputSubImage->getImagePointer(ImageVector((p.x - 1) + x, (p.y - 1) + y, 0, 0, 0, 0));
							if (*kernelVoxel2 >= counter)
							{
								auto a = 0;
								
							}
							kernelVoxel2 = outputSubImage->getImagePointer(ImageVector((p.x - 1) + x, (p.y - 1) + y, 0, 0, 0, 0));
							T* thisKernelVoxel = outputSubImage->getImagePointer(ImageVector(p.x, p.y, 0, 0, 0, 0));
							if (MLAbs(*inVoxel0 - *kernelVoxel1) <= threshold && (p.x != (p.x - 1) + x && p.y != (p.y - 1) + y) && *inVoxel0 != 0)
							{
								if (*thisKernelVoxel == *kernelVoxel2 && *kernelVoxel2 != 0 && *thisKernelVoxel != 0)
								{
									continue;
								}
								else if (*kernelVoxel2 != 0 && *thisKernelVoxel != 0 )
								{
									
									
									if (areas.size() <= *kernelVoxel2)
										auto a = 0;
									if (areas.size() <= *thisKernelVoxel)
										auto a = 0;
									std::vector<std::tuple<int, int, MLint>> inbetweenVector;
									inbetweenVector.reserve(areas[*kernelVoxel2].size() + areas[*thisKernelVoxel].size());
									inbetweenVector.insert(inbetweenVector.end(), areas[*kernelVoxel2].begin(), areas[*kernelVoxel2].end());
									inbetweenVector.insert(inbetweenVector.end(), areas[*thisKernelVoxel].begin(), areas[*thisKernelVoxel].end());

									if (areas.size() <= *thisKernelVoxel)
										auto a = 0;
									areas[*kernelVoxel2] = inbetweenVector;

									
									

									if (areas.size() <= *kernelVoxel2)
										auto a = 0;
									if (areas.size() <= *thisKernelVoxel)
										auto a = 0;
									


									for (int i = 0; i < areas[counter-1].size(); i++)
									{
										if (areas.size() <= counter-1)
											auto a = 0;
										std::tuple<int, int, MLint> t = areas[counter-1][i];
										T* voxel = outputSubImage->getImagePointer(ImageVector(std::get<0>(t), std::get<1>(t), 0, 0, 0, 0));
										std::get<2>(t) = *thisKernelVoxel;
										afdasdf
											//hier gaat het fout (of heir net boven), mogelijk word foute verwijderd of op foute plek de nieuwe vector neergezet en waardes veranderen is te laat
										*voxel = *thisKernelVoxel;
										//OutputImage[std::get<0>(t)][std::get<1>(t)] = *kernelVoxel2;
									}
									for (int i = 0; i < areas[*thisKernelVoxel].size(); i++)
									{
										if (areas.size() <= *thisKernelVoxel)
											auto a = 0;
										std::tuple<int, int, MLint> t = areas[*thisKernelVoxel][i];

										T* voxel = outputSubImage->getImagePointer(ImageVector(std::get<0>(t), std::get<1>(t), 0, 0, 0, 0));

										*voxel = *kernelVoxel2;
										//OutputImage[std::get<0>(t)][std::get<1>(t)] = value;
									}
								
									areas.erase(areas.begin() + *thisKernelVoxel);

									std::sort(areas.begin(), areas.end(), sortFunction);
									counter--;


								
								}
								else if (*kernelVoxel2 != 0)
								{
									if (areas.size() <= *kernelVoxel2)
										auto a = 0;
									areas[*kernelVoxel2].push_back(std::make_tuple(p.x, p.y, *kernelVoxel2));
									*outVoxel = *kernelVoxel2;
									//OutputImage[p.x][p.y] = *kernelVoxel2;

								}
								else if (*thisKernelVoxel != 0)
								{
									if (areas.size() <= *thisKernelVoxel)
										auto a = 0;
									areas[*thisKernelVoxel].push_back(std::make_tuple((p.x - 1) + x, (p.y - 1) + y, *thisKernelVoxel));
									*kernelVoxel2 = *thisKernelVoxel;
								//	OutputImage[(p.x - 1) + x][(p.y - 1) + y] = *thisKernelVoxel;
								}
								else
								{
									areas.push_back(dummy);
									if (areas.size() <= counter)
										auto a = 0;
									areas[counter].push_back(std::make_tuple(p.x, p.y,counter));
									areas[counter].push_back(std::make_tuple((p.x - 1) + x, (p.y - 1) + y, counter));
									*outVoxel = counter;
									//OutputImage[p.x][p.y] = counter;
									*kernelVoxel2 = counter;
									//OutputImage[(p.x - 1) + x][(p.y - 1) + y] = counter;
									counter++;
								}
							}
						}
					}
				}
            }
          }
        }
      }
    }
  }
}

ML_END_NAMESPACE