//----------------------------------------------------------------------------------
//! Dynamic library and runtime type system initialization.
/*!
// \file    
// \author  Jasper
// \date    2015-06-12
*/
//----------------------------------------------------------------------------------


#ifndef __DIAGAreaGrowInit_H
#define __DIAGAreaGrowInit_H


ML_START_NAMESPACE

//! Calls init functions of all modules to add their types to the runtime type
//! system of the ML.
int DIAGAreaGrowInit();

ML_END_NAMESPACE

#endif // __DIAGAreaGrowInit_H
