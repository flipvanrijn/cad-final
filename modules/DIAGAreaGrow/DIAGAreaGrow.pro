# -----------------------------------------------------------------------------
# DIAGAreaGrow project profile
#
# \file
# \author  Jasper
# \date    2015-06-12
# -----------------------------------------------------------------------------


TEMPLATE   = lib

TARGET     = DIAGAreaGrow

DESTDIR    = ../../../lib
DLLDESTDIR = ../../../lib

# Set high warn level (warn 4 on MSVC)
WARN = HIGH

# Add used projects here (see included pri files below for available projects)
CONFIG += dll ML

MLAB_PACKAGES += AddModule_CADCourse \
                 MeVisLab_Standard

# make sure that this file is included after CONFIG and MLAB_PACKAGES
include ($(MLAB_MeVis_Foundation)/Configuration/IncludePackages.pri)

DEFINES += DIAGAREAGROW_EXPORTS

# Enable ML deprecated API warnings. To completely disable the deprecated API, change WARN to DISABLE.
DEFINES += ML_WARN_DEPRECATED

HEADERS += \
    DIAGAreaGrowInit.h \
    DIAGAreaGrowSystem.h \
    mlAreaGrow.h \

SOURCES += \
    DIAGAreaGrowInit.cpp \
    mlAreaGrow.cpp \