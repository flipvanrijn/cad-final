//----------------------------------------------------------------------------------
//! Dynamic library and runtime type system initialization.
/*!
// \file    
// \author  Flip
// \date    2015-05-15
*/
//----------------------------------------------------------------------------------


// Local includes
#include "DIAGCombineOpticDiskComponentsSystem.h"

// Include definition of ML_INIT_LIBRARY.
#include <mlLibraryInitMacros.h>

// Include all module headers ...
#include "mlCombineOpticDiskComponents.h"


ML_START_NAMESPACE

//----------------------------------------------------------------------------------
//! Calls init functions of all modules to add their types to the runtime type
//! system of the ML.
//----------------------------------------------------------------------------------
int DIAGCombineOpticDiskComponentsInit()
{
  // Add initClass calls from modules here.
  CombineOpticDiskComponents::initClass();

  return 1;
}

ML_END_NAMESPACE


//! Calls the init method implemented above during load of shared library.
ML_INIT_LIBRARY(DIAGCombineOpticDiskComponentsInit)