# -----------------------------------------------------------------------------
# DIAGCombineOpticDiskComponents project profile
#
# \file
# \author  Flip
# \date    2015-05-15
# -----------------------------------------------------------------------------


TEMPLATE   = lib

TARGET     = DIAGCombineOpticDiskComponents

DESTDIR    = ../../../lib
DLLDESTDIR = ../../../lib

# Set high warn level (warn 4 on MSVC)
WARN = HIGH

# Add used projects here (see included pri files below for available projects)
CONFIG += dll ML

MLAB_PACKAGES += DIAG_CADStudent \
                 MeVisLab_Standard

# make sure that this file is included after CONFIG and MLAB_PACKAGES
include ($(MLAB_MeVis_Foundation)/Configuration/IncludePackages.pri)

DEFINES += DIAGCOMBINEOPTICDISKCOMPONENTS_EXPORTS

# Enable ML deprecated API warnings. To completely disable the deprecated API, change WARN to DISABLE.
DEFINES += ML_WARN_DEPRECATED

HEADERS += \
    DIAGCombineOpticDiskComponentsInit.h \
    DIAGCombineOpticDiskComponentsSystem.h \
    mlCombineOpticDiskComponents.h \

SOURCES += \
    DIAGCombineOpticDiskComponentsInit.cpp \
    mlCombineOpticDiskComponents.cpp \