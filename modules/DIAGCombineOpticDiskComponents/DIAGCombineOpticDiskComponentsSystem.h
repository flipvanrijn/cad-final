//----------------------------------------------------------------------------------
//! Project global and OS specific declarations.
/*!
// \file    
// \author  Flip
// \date    2015-05-15
*/
//----------------------------------------------------------------------------------


#ifndef __DIAGCombineOpticDiskComponentsSystem_H
#define __DIAGCombineOpticDiskComponentsSystem_H


// DLL export macro definition.
#ifdef DIAGCOMBINEOPTICDISKCOMPONENTS_EXPORTS
  // Use the DIAGCOMBINEOPTICDISKCOMPONENTS_EXPORT macro to export classes and functions.
  #define DIAGCOMBINEOPTICDISKCOMPONENTS_EXPORT ML_LIBRARY_EXPORT_ATTRIBUTE
#else
  // If included by external modules, exported symbols are declared as import symbols.
  #define DIAGCOMBINEOPTICDISKCOMPONENTS_EXPORT ML_LIBRARY_IMPORT_ATTRIBUTE
#endif


#endif // __DIAGCombineOpticDiskComponentsSystem_H
