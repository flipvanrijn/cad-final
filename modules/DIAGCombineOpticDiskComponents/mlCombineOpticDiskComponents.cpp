//----------------------------------------------------------------------------------
//! The ML module class CombineOpticDiskComponents.
/*!
// \file   
// \author  Flip
// \date    2015-05-15
//
// 
*/
//----------------------------------------------------------------------------------

// Local includes
#include "mlCombineOpticDiskComponents.h"


ML_START_NAMESPACE

//! Implements code for the runtime type system of the ML
ML_MODULE_CLASS_SOURCE(CombineOpticDiskComponents, Module);

//----------------------------------------------------------------------------------

CombineOpticDiskComponents::CombineOpticDiskComponents() : Module(1, 1)
{
  // Suppress calls of handleNotification on field changes to
  // avoid side effects during initialization phase.
  handleNotificationOff();

  // Add fields to the module and set their values.
  _ComponentDistFld = addInt("ComponentDist", 60);
  _XFld = addInt("X", 0);
  _YFld = addInt("Y", 0);

  // Reactivate calls of handleNotification on field changes.
  handleNotificationOn();


  // Activate inplace data buffers for output outputIndex and input inputIndex.
  // setOutputImageInplace(outputIndex, inputIndex);

  // Activate page data bypass from input inputIndex to output outputIndex.
  // Note that the module must still be able to calculate the output image.
  // setBypass(outputIndex, inputIndex);

}

//----------------------------------------------------------------------------------

void CombineOpticDiskComponents::handleNotification(Field* field)
{
  // Handle changes of module parameters and input image fields here.
  bool touchOutputs = false;
  if (isInputImageField(field))
  {
    touchOutputs = true;
  }
  else if (field == _ComponentDistFld)
  {
    touchOutputs = true;
  }
  else if (field == _XFld)
  {
    touchOutputs = true;
  }
  else if (field == _YFld)
  {
    touchOutputs = true;
  }

  if (touchOutputs) 
  {
    // Touch all output image fields to notify connected modules.
    touchOutputImageFields();
  }
}

//----------------------------------------------------------------------------------

void CombineOpticDiskComponents::calculateOutputImageProperties(int /*outputIndex*/, PagedImage* outputImage)
{
  // Change properties of output image outputImage here whose
  // defaults are inherited from the input image 0 (if there is one).
}

//----------------------------------------------------------------------------------

SubImageBox CombineOpticDiskComponents::calculateInputSubImageBox(int inputIndex, const SubImageBox& outputSubImageBox, int outputIndex)
{
  // Return region of input image inputIndex needed to compute region
  // outSubImgBox of output image outputIndex.
  return outputSubImageBox;
}


//----------------------------------------------------------------------------------

ML_CALCULATEOUTPUTSUBIMAGE_NUM_INPUTS_1_CPP(CombineOpticDiskComponents);

template <typename T>
void CombineOpticDiskComponents::calculateOutputSubImage(TSubImage<T>* outputSubImage, int outputIndex
  , TSubImage<T>* inputSubImage0
  )
{
  std::map<int, int> hist = this->calculateHistogram(inputSubImage0);

  int largestComponent = 0;
  int largestSize = 0;
  for (std::map<int, int>::iterator it = hist.begin(); it != hist.end(); ++it) {
    if (largestSize < it->second) {
      largestComponent = it->first;
      largestSize = it->second;
    }
  }
  int meanXLargest = static_cast<double>(this->spatialCoordinates[largestComponent].at(0) + this->spatialCoordinates[largestComponent].at(1)) / 2.0;
  int meanYLargest = static_cast<double>(this->spatialCoordinates[largestComponent].at(2) + this->spatialCoordinates[largestComponent].at(3)) / 2.0;
  std::vector<int> componentsToMerge;
  componentsToMerge.push_back(largestComponent);

  for (std::map<int, std::vector<int>>::iterator it = this->spatialCoordinates.begin(); it != this->spatialCoordinates.end(); ++it) {
    int meanXComponent = static_cast<double>(it->second.at(0) + it->second.at(1)) / 2.0;
    int meanYComponent = static_cast<double>(it->second.at(2) + it->second.at(3)) / 2.0;
    if (sqrt(pow(meanXLargest - meanXComponent, 2.0) + pow(meanYLargest - meanYComponent, 2.0)) < _ComponentDistFld->getIntValue()) {
      componentsToMerge.push_back(it->first);
    }
  }

  int minX = INT_MAX;
  int maxX = 0;
  int minY = INT_MAX;
  int maxY = 0;
  for (std::vector<int>::iterator it = componentsToMerge.begin(); it != componentsToMerge.end(); ++it) {
    std::vector<int> coords = this->spatialCoordinates[*it];
    if (coords.at(0) < minX) {
      minX = coords.at(0);
    }
    if (coords.at(1) > maxX) {
      maxX = coords.at(1);
    }
    if (coords.at(2) < minY) {
      minY = coords.at(2);
    }
    if (coords.at(3) > maxY) {
      maxY = coords.at(3);
    }
  }

  _XFld->setIntValue(static_cast<int>(static_cast<double>(minX + maxX) / 2.0));
  _YFld->setIntValue(static_cast<int>(static_cast<double>(minY + maxY) / 2.0));

  // Compute sub-image of output image outputIndex from input sub-images.

  // Clamp box of output image against image extent to avoid that unused areas are processed.
  const SubImageBox validOutBox = outputSubImage->getValidRegion();

  // Process all voxels of the valid region of the output page.
  ImageVector p;
  for (p.u = validOutBox.v1.u; p.u <= validOutBox.v2.u; ++p.u) {
    for (p.t = validOutBox.v1.t; p.t <= validOutBox.v2.t; ++p.t) {
      for (p.c = validOutBox.v1.c; p.c <= validOutBox.v2.c; ++p.c) {
        for (p.z = validOutBox.v1.z; p.z <= validOutBox.v2.z; ++p.z) {
          for (p.y = validOutBox.v1.y; p.y <= validOutBox.v2.y; ++p.y) {

            p.x = validOutBox.v1.x;
            // Get pointers to row starts of input and output sub-images.
            const T* inVoxel0 = inputSubImage0->getImagePointer(p);

            T*  outVoxel = outputSubImage->getImagePointer(p);

            const MLint rowEnd = validOutBox.v2.x;

            // Process all row voxels.
            for (; p.x <= rowEnd; ++p.x, ++outVoxel, ++inVoxel0)
            {
              std::vector<int>::iterator it = std::find(componentsToMerge.begin(), componentsToMerge.end(), *inVoxel0);
              if (*inVoxel0 == largestComponent || it != componentsToMerge.end()) {
                *outVoxel = 1;
              }
              else {
                *outVoxel = 0;
              }
            }
          }
        }
      }
    }
  }
}

template <typename T>
std::map<int, int> CombineOpticDiskComponents::calculateHistogram(TSubImage<T>* inputSubImage0)
{
  std::map<int, int> _histogram;

  const SubImageBox validOutBox = inputSubImage0->getValidRegion();

  // Process all voxels of the valid region of the output page.
  ImageVector p;
  for (p.u = validOutBox.v1.u; p.u <= validOutBox.v2.u; ++p.u) {
    for (p.t = validOutBox.v1.t; p.t <= validOutBox.v2.t; ++p.t) {
      for (p.c = validOutBox.v1.c; p.c <= validOutBox.v2.c; ++p.c) {
        for (p.z = validOutBox.v1.z; p.z <= validOutBox.v2.z; ++p.z) {
          for (p.y = validOutBox.v1.y; p.y <= validOutBox.v2.y; ++p.y) {

            p.x = validOutBox.v1.x;
            // Get pointers to row starts of input and output sub-images.
            const T* inVoxel0 = inputSubImage0->getImagePointer(p);

            const MLint rowEnd = validOutBox.v2.x;

            // Process all row voxels.
            for (; p.x <= rowEnd; ++p.x, ++inVoxel0)
            {
              if (*inVoxel0 != 0)
              {
                std::map<int, int>::iterator it = _histogram.find(*inVoxel0);
                if (it != _histogram.end()) {
                  _histogram[*inVoxel0] = _histogram[*inVoxel0] + 1;
                  std::vector<int> coords = this->spatialCoordinates[*inVoxel0];
                  if (p.x > coords[1]) {
                    coords[1] = p.x;
                  }
                  else if (p.x < coords[0]) {
                    coords[0] = p.x;
                  }
                  else if (p.y > coords[3]) {
                    coords[3] = p.y;
                  }
                  else if (p.y < coords[2]) {
                    coords[2] = p.y;
                  }

                  this->spatialCoordinates[*inVoxel0] = coords;
                }
                else {
                  _histogram[*inVoxel0] = 1;
                  int arCoords[4] = { p.x, p.x, p.y, p.y };
                  std::vector<int> coords(arCoords, arCoords + 4);
                  this->spatialCoordinates[*inVoxel0] = coords;
                }
              }
            }
          }
        }
      }
    }
  }

  return _histogram;
}

ML_END_NAMESPACE