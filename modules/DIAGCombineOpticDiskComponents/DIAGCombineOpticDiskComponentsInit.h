//----------------------------------------------------------------------------------
//! Dynamic library and runtime type system initialization.
/*!
// \file    
// \author  Flip
// \date    2015-05-15
*/
//----------------------------------------------------------------------------------


#ifndef __DIAGCombineOpticDiskComponentsInit_H
#define __DIAGCombineOpticDiskComponentsInit_H


ML_START_NAMESPACE

//! Calls init functions of all modules to add their types to the runtime type
//! system of the ML.
int DIAGCombineOpticDiskComponentsInit();

ML_END_NAMESPACE

#endif // __DIAGCombineOpticDiskComponentsInit_H
